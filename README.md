# Spring Security SAML & ADFS prototype #

An example project for demonstration of the implementation of
[Spring Security SAML](https://projects.spring.io/spring-security/) with *ADFS* as
an *IdP* (Identity Provider).

The project has following parameters:

* Build via *Spring Framework* and *Spring Security* version **5**.
* Configured via *Java configuration* only.
* Use of *SHA-256* for SAML message signing.

## How to run the example

1. Clone the repository.
1. Run the command `gradle tomcatRun`.
1. Navigate to the URL <https://your-ip-address-or-hostname/deep-thought/saml/metadata> and download a XML metadata file.
1. Copy the metadata file to the *ADFS* server and register it as a new *Relying Party Trust*.
1. Define a *Claim Rule* for *Name ID* and *Group* attributes.
1. Open a browser on URL <https://your-ip-address-or-hostname/deep-thought/user> to proceed with *SSO* (Single Sign-On).
1. Open a browser on URL <https://your-ip-address-or-hostname/deep-thought/saml/logout> to proceed with *SLO* (Single Logout).
1. Browse the source code.

## License ##

The **blog-spring-security** project is published under [BSD 3-Clause](http://opensource.org/licenses/BSD-3-Clause) license.
