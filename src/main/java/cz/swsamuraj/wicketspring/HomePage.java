package cz.swsamuraj.wicketspring;

import cz.swsamuraj.wicketspring.pages.UserPage;
import cz.swsamuraj.wicketspring.websocket.PersonPage;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.link.Link;

public class HomePage extends WebPage {

	public HomePage() {
		add(new Link<Void>("userPage") {
			@Override
			public void onClick() {
				setResponsePage(UserPage.class);
			}
		});
		add(new Link<Void>("personPage") {
			@Override
			public void onClick() {
				setResponsePage(PersonPage.class);
			}
		});
    }
}
