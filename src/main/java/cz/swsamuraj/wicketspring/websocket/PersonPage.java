package cz.swsamuraj.wicketspring.websocket;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.springframework.security.core.context.SecurityContextHolder;

public class PersonPage extends WebPage {

    public PersonPage() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        add(new Label("helloLabel", "Hello, Wicket & Spring!"));
        add(new PersonPanel("personPanel42", "42"));
        add(new PersonPanel("personPanel12", "12"));
    }

}
