package cz.swsamuraj.wicketspring.websocket;

import org.apache.wicket.protocol.ws.api.message.IWebSocketPushMessage;

public class WebSocketPersonMessage implements IWebSocketPushMessage {

    private String personID;

    public WebSocketPersonMessage(String personID) {
        this.personID = personID;
    }

    public String getPersonID() {
        return personID;
    }

    @Override
    public String toString() {
        return "Person ID: " + personID;
    }
}
