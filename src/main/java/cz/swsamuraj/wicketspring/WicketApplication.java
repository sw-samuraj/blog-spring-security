package cz.swsamuraj.wicketspring;

import cz.swsamuraj.wicketspring.pages.UserPage;
import cz.swsamuraj.wicketspring.security.AuthenticatedSession;
import cz.swsamuraj.wicketspring.security.LoginPage;
import cz.swsamuraj.wicketspring.websocket.PersonPage;
import org.apache.wicket.authroles.authentication.AbstractAuthenticatedWebSession;
import org.apache.wicket.authroles.authentication.AuthenticatedWebApplication;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.CsrfPreventionRequestCycleListener;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletRegistration;

public class WicketApplication extends AuthenticatedWebApplication {

    @Override
    public Class<? extends WebPage> getHomePage() {
        return HomePage.class;
    }

    @Override
    protected void init() {
        super.init();

        WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
        getComponentInstantiationListeners().add(new SpringComponentInjector(this, ctx));

        ServletRegistration.Dynamic dispatcherServlet = getServletContext().addServlet(
                "dispatcherServlet",
                new DispatcherServlet(ctx));
        dispatcherServlet.setLoadOnStartup(1);
        dispatcherServlet.addMapping("/rest/*");

        mountPage("login", LoginPage.class);
        mountPage("user", UserPage.class);
        mountPage("person", PersonPage.class);
    }

    @Override
    protected Class<? extends AbstractAuthenticatedWebSession> getWebSessionClass() {
        return AuthenticatedSession.class;
    }

    @Override
    protected Class<? extends WebPage> getSignInPageClass() {
        return LoginPage.class;
    }
}
