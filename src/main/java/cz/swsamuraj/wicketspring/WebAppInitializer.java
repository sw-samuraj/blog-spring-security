package cz.swsamuraj.wicketspring;

import cz.swsamuraj.wicketspring.security.SecurityConfiguration;
import cz.swsamuraj.wicketspring.ws.RestConfiguration;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

public class WebAppInitializer extends AbstractSecurityWebApplicationInitializer {

    public WebAppInitializer() {
        super(SecurityConfiguration.class, RestConfiguration.class);
    }

}
