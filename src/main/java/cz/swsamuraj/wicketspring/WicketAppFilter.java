package cz.swsamuraj.wicketspring;

import org.apache.wicket.protocol.ws.javax.JavaxWebSocketFilter;

import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;

@WebFilter(value = "/*",
           initParams = {
                   @WebInitParam(name = "applicationClassName",
                                 value = "cz.swsamuraj.wicketspring.WicketApplication")
           })
public class WicketAppFilter extends JavaxWebSocketFilter {
}
