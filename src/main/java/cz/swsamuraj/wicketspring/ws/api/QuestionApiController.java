package cz.swsamuraj.wicketspring.ws.api;

import cz.swsamuraj.wicketspring.ws.model.Answer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

@Controller
public class QuestionApiController implements QuestionApi {

    private static final Logger logger = LogManager.getLogger(QuestionApiController.class);

    @Override
    public ResponseEntity<Answer> getAnswer() {
        Answer answer = new Answer();
        answer.setAnswer(42);

        logger.debug("Answer:\n" + answer);

        return new ResponseEntity<Answer>(answer, HttpStatus.OK);
    }
}
