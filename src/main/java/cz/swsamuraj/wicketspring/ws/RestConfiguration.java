package cz.swsamuraj.wicketspring.ws;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
@ComponentScan("cz.swsamuraj.wicketspring.ws.api")
public class RestConfiguration implements WebMvcConfigurer {

    @Bean
    public ObservableCache observableCache() {
        return new ObservableCache();
    }

}
