package cz.swsamuraj.wicketspring.security;

import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.HiddenField;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.protocol.http.servlet.ServletWebRequest;
import org.apache.wicket.request.flow.RedirectToUrlException;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.savedrequest.SavedRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class LoginPage extends WebPage {

    public LoginPage() {
        add(new LoginForm("loginForm"));
    }

    private class LoginForm extends Form<LoginForm> {

        private String username;
        private String password;

        LoginForm(String id) {
            super(id);

            setModel(new CompoundPropertyModel<LoginForm>(this));
            add(new TextField("username"));
            add(new PasswordTextField("password"));

            CsrfToken csrf = csrfToken();
            add(new HiddenField<String>("_csrf", Model.of(csrf.getToken())));
        }

        @Override
        protected void onSubmit() {
            HttpServletRequest request = (HttpServletRequest) getRequest().getContainerRequest();
            String originalUrl = originalUrl(request.getSession());
            AuthenticatedWebSession session = AuthenticatedWebSession.get();
            boolean signIn = session.signIn(username, password);

            if (signIn) {
                throw new RedirectToUrlException(originalUrl);
            }
        }

        private CsrfToken csrfToken() {
            ServletWebRequest servletWebRequest = (ServletWebRequest) getRequest();
            HttpServletRequest request = servletWebRequest.getContainerRequest();

            return (CsrfToken) request.getAttribute("_csrf");
        }

        private String originalUrl(HttpSession session) {
            SavedRequest request = (SavedRequest) session.getAttribute("SPRING_SECURITY_SAVED_REQUEST");

            return request.getRedirectUrl();
        }
    }
}
