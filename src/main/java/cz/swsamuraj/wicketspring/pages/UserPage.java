package cz.swsamuraj.wicketspring.pages;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.opensaml.saml2.core.Attribute;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.saml.SAMLCredential;

import java.util.Arrays;

public class UserPage extends WebPage {

    public UserPage() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        SAMLCredential credential = (SAMLCredential) authentication.getCredentials();

        add(new Label("name", authentication.getName()));
        add(new Label("principal", authentication.getPrincipal().toString()));
        add(new Label("nameID", credential.getNameID().getValue()));
        add(new Label("remoteEntityID", credential.getRemoteEntityID()));

        add(new ListView<Attribute>("attributes", credential.getAttributes()) {
            @Override
            protected void populateItem(ListItem<Attribute> item) {
                String attributeName = item.getModelObject().getName();
                String[] attributes = credential.getAttributeAsStringArray(attributeName);

                item.add(new Label("attributeName", attributeName));
                item.add(new ListView<String>("attributeValues", Arrays.asList(attributes)) {
                    @Override
                    protected void populateItem(ListItem<String> item) {
                        item.add(new Label("attributeValue", item.getModel()));
                    }
                });
            }
        });
    }

}
